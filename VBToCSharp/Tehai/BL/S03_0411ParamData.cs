﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 手仕舞い情報登録パラメータクラス
	/// </summary>
	public class S03_0411ParamData
	{
		/// <summary>
		/// コースコード
		/// </summary>
		/// <returns></returns>
		public string CrsCd {get; set;}

		/// <summary>
		/// 仕入先コード
		/// </summary>
		/// <returns></returns>
		public string SiireSakiCd {get; set;}

		/// <summary>
		/// 仕入先枝番
		/// </summary>
		/// <returns></returns>
		public string SiireSakiNo {get; set;}

		/// <summary>
		/// 日次
		/// </summary>
		/// <returns></returns>
		public int Daily {get; set;}

		/// <summary>
		/// 通知方法
		/// </summary>
		/// <returns></returns>
		public string NotificationHoho {get; set;}
		/// <summary>
		/// 出発日
		/// </summary>
		/// <returns></returns>
		public string SyuptDayStr {get; set;}

		/// <summary>
		/// 手仕舞いデータ
		/// </summary>
		/// <returns></returns>
		public TejimaiParamData TejimaiParamData {get; set;}
	}

}