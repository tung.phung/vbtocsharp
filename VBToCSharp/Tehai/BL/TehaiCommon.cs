﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hatobus.ReservationManagementSystem.Yoyaku;
using Hatobus.ReservationManagementSystem.Zaseki;
using Microsoft.VisualBasic; // Install-Package Microsoft.VisualBasic
using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic

public partial class TehaiCommon
{

    #region  定数／変数

    // 営ブロック定
    public int eiBlockRegular { get; set; } = 0;

    // 空席数（定）
    public int kusekiRegular { get; set; } = 0;

    // 定席数（定）
    public int teisekiRegular { get; set; } = 0;

    // 営ブロック補
    public int eiBlockHo { get; set; } = 0;

    // 空席数（補）
    public int kusekiHo { get; set; } = 0;

    // 定席数（補）
    public int teisekiHo { get; set; } = 0;

    // 引渡し用座席イメージ（バス情報）
    public DataTable zasekiBusInfoData { get; set; } = default;

    // 引渡し用座席イメージ（座席情報）
    public DataTable zasekiInfoData { get; set; } = default;

    // 画面側でのリザルトチェック用
    public int moveGosyaResult { get; set; } = 0;

    #endregion

    #region 列挙
    /// <summary>
    /// マルチコンボの種類を指定
    /// </summary>
    /// <remarks></remarks>
    public enum multiComboType : int
    {
        carrier = 0,
        sonota
    }

    /// <summary>
    /// 使用中フラグの処理対象を指定
    /// </summary>
    /// <remarks></remarks>
    public enum targetTableType : int
    {
        crsMst,
        zasekiImage
    }

    /// <summary>
    /// 使用中フラグの更新方法を指定
    /// </summary>
    /// <remarks></remarks>
    public enum updateModeType : int
    {
        @lock,
        reject,
        befReject
    }

    /// <summary>
    /// 表示メッセージを指定
    /// </summary>
    /// <remarks></remarks>
    public enum messageType : int
    {
        [Value("E90_050")]
        errorMsg,
        [Value("I03_022")]
        infoMeg
    }

    /// <summary>
    /// 使用中フラグの処理対象を指定
    /// </summary>
    /// <remarks></remarks>
    public sealed partial class usingFlgTableType
    {
        public const string SYUPT_DAY = "SYUPT_DAY";
        public const string CRS_CD = "CRS_CD";
        public const string GOUSYA = "GOUSYA";
        public const string BUS_RESERVE_CD = "BUS_RESERVE_CD";
        public const string USING_FLG = "USING_FLG";
        public const string SYSTEM_UPDATE_DAY = "SYSTEM_UPDATE_DAY";
        public const string SYSTEM_UPDATE_PGMID = "SYSTEM_UPDATE_PGMID";
        public const string SYSTEM_UPDATE_PERSON_CD = "SYSTEM_UPDATE_PERSON_CD";
    }

    #endregion

    #region メソッド
    /// <summary>
    /// 文字項目セル設定
    /// </summary>
    /// <param name="maxLen"></param>
    /// <param name="imeMode"></param>
    /// <param name="format"></param>
    /// <returns></returns>
    public TextBoxEx getTextEx(int maxLen, ImeMode imeMode, string format)
    {
        var textEx = new TextBoxEx();
        textEx.Text = string.Empty;
        textEx.ImeMode = imeMode;
        textEx.MaxLength = maxLen;
        textEx.MaxLengthUnit = GrapeCity.Win.Editors.LengthUnit.Byte;
        textEx.Format = format;
        return textEx;
    }

    /// <summary>
    /// 数値項目セル設定
    /// </summary>
    /// <param name="maxDigits"></param>
    /// <param name="commaSeparator"></param>
    /// <param name="minValue"></param>
    /// <returns></returns>
    public NumberEx getNumberEx(int maxDigits, bool commaSeparator = true, decimal? minValue = default)

    {
        var numEx = new NumberEx();
        numEx.Fields.IntegerPart.MaxDigits = maxDigits;
        numEx.Fields.DecimalPart.MaxDigits = 0;
        if (commaSeparator == true)
        {
            numEx.Fields.IntegerPart.GroupSeparator = ',';
        }
        else
        {
            numEx.Fields.IntegerPart.GroupSeparator = Conversions.ToChar("");
        }

        numEx.ImeMode = Windows.Forms.ImeMode.Disable;
        numEx.Spin.SpinOnKeys = false;
        numEx.AllowDeleteToNull = true;

        // 最小値の設定
        if (minValue is null)
        {
            // 指定がない場合は、(桁数 * -1) を設定 (ex) 3桁の場合：-999
            minValue = Conversions.ToDecimal(string.Concat(Enumerable.Repeat("9", maxDigits))) * -1;
        }

        numEx.MinValue = Conversions.ToDecimal(minValue);
        return numEx;
    }

    /// <summary>
    /// 場所のコンボセル設定
    /// </summary>
    /// <param name="_comboData"></param>
    /// <param name="_multiType"></param>
    /// <returns></returns>
    public C1.Win.C1FlexGrid.MultiColumnDictionary getPlaceMultiCombo(DataTable _comboData, multiComboType _multiType)
    {
        C1.Win.C1FlexGrid.MultiColumnDictionary multiCmb;
        switch (_multiType)
        {
            case multiComboType.carrier:
                {
                    multiCmb = new C1.Win.C1FlexGrid.MultiColumnDictionary(_comboData, ComboBoxCdType.CODE_VALUE.ToString, new string[] { ComboBoxCdType.CODE_VALUE.ToString, ComboBoxCdType.CODE_NAME.ToString }, ComboBoxCdType.CODE_NAME);
                    break;
                }

            default:
                {
                    multiCmb = new C1.Win.C1FlexGrid.MultiColumnDictionary(_comboData, ComboBoxCdType.CODE_VALUE.ToString, new string[] { ComboBoxCdType.CODE_VALUE.ToString, ComboBoxCdType.CODE_NAME.ToString, ComboBoxCdType.NAIYO_1.ToString }, ComboBoxCdType.NAIYO_1);
                    break;
                }
        }

        return multiCmb;
    }

    /// <summary>
    /// グリッドに設置したコンボボックスへデータを格納するListを作成する
    /// </summary>
    /// <param name="dataTableValue"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public Dictionary<string, string> getComboboxDataNotSort(DataTable dataTableValue, bool nullRecord = false)
    {
        var returnValue = new Dictionary<string, string>();
        if (nullRecord == true)
        {
            returnValue.Add("", "");
        }

        for (int idxCountRow = 0, loopTo = dataTableValue.Rows.Count - 1; idxCountRow <= loopTo; idxCountRow++)
            returnValue.Add(dataTableValue.Rows(idxCountRow).Item(ComboBoxCdType.CODE_VALUE.ToString).ToString, dataTableValue.Rows(idxCountRow).Item(ComboBoxCdType.CODE_NAME.ToString).ToString);
        return returnValue;
    }

    /// <summary>
    /// 使用中フラグ更新処理
    /// </summary>
    public int updateUsingFlg(targetTableType tgtMst, updateModeType updMode, DataTable prmDt, string prmName, int msgType, DateTime dtToday)
    {
        var dataAccess = new DaityoMain_DA();
        int resultCnt;
        resultCnt = dataAccess.executeUsingFlg(prmDt, tgtMst, updMode, dtToday, prmName);
        if (updMode == updateModeType.@lock)
        {
            if (prmDt.Select(string.Format(usingFlgTableType.USING_FLG + "='{0}'", UsingFlg.Use)).Count > 0)
            {
                if (msgType == default)
                {
                }
                else
                {
                    CommonProcess.createFactoryMsg().messageDisp(getEnumAttrValue(Conversions.ToInteger(msgType)));
                }
            }
        }

        return resultCnt;
    }

    private string yoyakuUsingFlgCol = "YOYAKU_USING_FLG";

    /// <summary>
    /// 使用中フラグ更新処理
    /// </summary>
    public int updateUsingFlgYoyaku(updateModeType updMode, DataTable prmDt, int msgType)
    {
        var msgChgCls = new crsMessageChangeAfter();
        bool retResult = false;
        if (prmDt.Columns.Contains(yoyakuUsingFlgCol) == false)
        {
            prmDt.Columns.Add(yoyakuUsingFlgCol);
        }

        if (updMode == updateModeType.@lock)
        {
            if (prmDt.Select(string.Format(usingFlgTableType.USING_FLG + "='{0}'", UsingFlg.Use)).Count > 0)
            {
            }
            // 手配でロックのレコードに対して予約にもロックを実行する
            foreach (DataRow row in prmDt.Select(usingFlgTableType.USING_FLG + " IS NULL"))
            {
                retResult = msgChgCls.chkYoyakuUsingFlgByCrs(row(usingFlgTableType.CRS_CD).ToString, row(usingFlgTableType.GOUSYA).ToString, row(usingFlgTableType.SYUPT_DAY).ToString);
                if (retResult == true)
                {
                    row(yoyakuUsingFlgCol) = UsingFlg.Use;
                    row(usingFlgTableType.USING_FLG) = UsingFlg.Use;
                }
            }

            if (prmDt.Select(string.Format(yoyakuUsingFlgCol + "='{0}'", UsingFlg.Use)).Count > 0)
            {
                if (msgType == default)
                {
                }
                else
                {
                    CommonProcess.createFactoryMsg().messageDisp(getEnumAttrValue(Conversions.ToInteger(msgType)));
                }
            }
        }
        else if (updMode == updateModeType.reject)
        {
            foreach (DataRow row in prmDt.Select(usingFlgTableType.USING_FLG + " IS NULL AND " + yoyakuUsingFlgCol + " IS NULL"))
                retResult = msgChgCls.updYoyakuUsingFlgByCrs(row(usingFlgTableType.CRS_CD).ToString, row(usingFlgTableType.GOUSYA).ToString, row(usingFlgTableType.SYUPT_DAY).ToString);
        }
        else if (updMode == updateModeType.befReject)
        {
            foreach (DataRow row in prmDt.Select(string.Format(yoyakuUsingFlgCol + "='{0}'", UsingFlg.Use)))
                row(usingFlgTableType.USING_FLG) = UsingFlg.Unused;
        }

        return default;
    }

    /// <summary>
    /// 枝番から降車ヶ所エンティティの配列を取得
    /// </summary>
    /// <param name="edaBan"></param>
    /// <returns></returns>
    public int getKoshakashoIndex(int edaBan, EntityOperation entKoshakasho)
    {
        int returnValue = -1;

        // 枝番から降車ヶ所エンティティの配列を検索
        for (int idx = 0, loopTo = entKoshakasho.EntityData.Length - 1; idx <= loopTo; idx++)
        {
            if (entKoshakasho.EntityData(idx).edaban.Value is null)
            {
                returnValue = idx;
                break;
            }

            if (edaBan.Equals(Conversions.ToInteger(entKoshakasho.EntityData(idx).edaban.Value)))
            {
                returnValue = idx;
                break;
            }
        }

        return returnValue;
    }

    /// <summary>
    /// カンマ変換メソッド
    /// ※エンティティの金額の値をカンマ付の値に変換します。
    /// </summary>
    /// <returns></returns>
    /// <remarks></remarks>
    public string convertComma(int? _kingaku)
    {
        if (_kingaku is null)
        {
            return null;
        }
        else
        {
            return _kingaku.Value.ToString("#,0");
        }
    }

    /// <summary>
    /// NULL数値変換 (Nothing の場合、定義値を返す)
    /// </summary>
    /// <param name="pObj"></param>
    /// <param name="pInitValue"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public int? nnvl_int(object pObj, int? pInitValue = default)
    {
        var returnValue = pInitValue;
        if (!Information.IsDBNull(pObj) && pObj is object && !((Conversions.ToString(pObj) ?? "") == (string.Empty ?? "")))
        {
            if (Information.IsNumeric(pObj))
            {
                return Conversions.ToInteger(pObj);
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 座席自動配置（空席再計算）を呼び出し値を取得
    /// </summary>
    /// <param name="_tblZasekiImage"></param>
    /// <param name="zasekiImageDt"></param>
    /// <returns>True:エラー / False:エラーなし</returns>
    public bool getKusekiNum(DataTable _tblZasekiImage, DataTable zasekiImageDt)
    {
        var kusekiNum = new Z0009();
        var setParam = new Z0009_Param();
        var getResult = new Z0009_Result();
        int blockOK = 0;

        // 定数初期化
        eiBlockRegular = 0;
        kusekiRegular = 0;
        teisekiRegular = 0;
        eiBlockHo = 0;
        kusekiHo = 0;
        teisekiHo = 0;

        // パラメータエンティティ設定 （座席イメージ（バス情報）と座席イメージ（座席情報））
        var zasekiImageEntity = zasekiImageDt.SetToEntity<TZasekiImageEntity>;
        setParam.BusInfo = zasekiImageEntity.EntityData(0);
        // setParam.ZasekiInfo = (SetToEntity(Of TZasekiImageInfoEntity)(_tblZasekiImage)).EntityData
        setParam.ZasekiInfo = SetToEntity<TZasekiImageInfoEntity>(_tblZasekiImage).EntityData.ToList;

        // Call 座席自動配置処理
        getResult = kusekiNum.Execute(setParam);

        // リザルトステータスを見てエラーを確認
        // 99でかえってきた場合
        if (getResult.Status != blockOK)
        {
            return false;
        }

        // 座席自動配置処理から受け取った値を設定
        // 営ブロック定
        eiBlockRegular = getResult.EiBLNumTeiseki;

        // 空席数（定）
        kusekiRegular = getResult.KusekiNumTeiseki;

        // 定席数（定）
        teisekiRegular = getResult.CapacityNumTeiseki;

        // 営ブロック補
        eiBlockHo = getResult.EiBLNumSub1F;

        // 空席数（補）
        kusekiHo = getResult.KusekiNumSub1F;

        // 定席数（補）
        teisekiHo = getResult.CapacityNumSub1F;
        return true;
    }

    /// <summary>
    /// 座席自動配置（車種・制御）を呼び出し値を取得
    /// </summary>
    /// <param name="crsLegBasicDt"></param>
    /// <param name="syoriKbn"></param>
    /// <param name="haitiJun"></param>
    /// <returns>リザルトコード</returns>
    public int getChangeCarKind(DataTable crsLegBasicDt, int syoriKbn, string haitiJun, OracleTransaction trn)
    {
        var changeCarKind = new Z0010();
        var setParam = new Z0010_Param();
        // Dim zasekiSyasyu As New ZasekiShashuHenko
        var getResult = new Z0010_Result();
        int checkFlg = 10;
        int seijo = 0;

        // 定数初期化
        eiBlockRegular = 0;
        kusekiRegular = 0;
        teisekiRegular = 0;
        eiBlockHo = 0;
        kusekiHo = 0;
        teisekiHo = 0;

        // パラメータ設定
        // チェックモードかどうかをチェック
        if (syoriKbn == checkFlg)
        {
            // チェックモード
            setParam.ProcessKbn = Z0010_Param.Z0010_Param_ProcessKbn.ProcessKbn_10;
        }
        else
        {
            // 更新モード
            setParam.ProcessKbn = Z0010_Param.Z0010_Param_ProcessKbn.ProcessKbn_30;
        }

        // 配置順 10:そのまま 20:偶奇数順 30:予約順
        // setParam.HaichiOrder = haitiJun

        // 出発日
        setParam.SyuptDay = Conversions.ToInteger(Strings.Replace(Conversions.ToString(crsLegBasicDt.Rows.Item(0).ItemArray(0)), "/", ""));

        // コースコード
        // setParam.CrsCd = crsLegBasicDt.Rows.Item(0).ItemArray(1).ToString

        // 号車
        setParam.Gousya = Conversions.ToInteger(crsLegBasicDt.Rows.Item(0).ItemArray(2));

        // バス指定コード
        setParam.BusReserveCd = crsLegBasicDt.Rows.Item(0).ItemArray(3).ToString;

        // 車種コード
        setParam.NewCarTypeCd = crsLegBasicDt.Rows.Item(0).ItemArray(4).ToString;

        // 座席指定区分
        // setParam.ZasekiReserveKbn = crsLegBasicDt.Rows.Item(0).ItemArray(5).ToString

        // 定員数／定席
        // zasekiSyasyu.CapacityNumTeiseki = CType(crsLegBasicDt.Rows.Item(0).ItemArray(6), Integer)

        // 定員数／補助･１Ｆ
        // zasekiSyasyu.CapacityNumSub1F = CType(crsLegBasicDt.Rows.Item(0).ItemArray(7), Integer)

        // 空席数／定席
        // zasekiSyasyu.KusekiNumTeiseki = CType(crsLegBasicDt.Rows.Item(0).ItemArray(8), Integer)

        // 空席数／補助･１Ｆ
        // zasekiSyasyu.KusekiNumSub1F = CType(crsLegBasicDt.Rows.Item(0).ItemArray(9), Integer)

        // 営BL数／定席
        // zasekiSyasyu.EiBLNumTeiseki = CType(crsLegBasicDt.Rows.Item(0).ItemArray(10), Integer)

        // 営BL数／補助･１Ｆ
        // zasekiSyasyu.EiBLNumSub1F = CType(crsLegBasicDt.Rows.Item(0).ItemArray(11), Integer)

        // トランザクション
        setParam.dbTran = trn;

        // Call 座席自動配置（車種・制御）
        getResult = changeCarKind.Execute(setParam);

        // リザルトステータスチェック
        // 0:正常終了 10:架空 11:使用中 12:残席数不足 19:レディース席満席 21:他理由取得不可 空席再計算エラー:91 パラメータエラー:99
        if (getResult.Status != 0)
        {
            return getResult.Status;
        }

        // 取得した値を設定
        // 座席自動配置処理から受け取った値を設定
        // 営ブロック定
        eiBlockRegular = getResult.EiBLNumTeiseki;

        // 空席数（定）
        kusekiRegular = getResult.KusekiNumTeiseki;

        // 定席数（定）
        teisekiRegular = getResult.CapacityNumTeiseki;

        // 営ブロック補
        eiBlockHo = getResult.EiBLNumSub1F;

        // 空席数（補）
        kusekiHo = getResult.KusekiNumSub1F;

        // 定席数（補）
        teisekiHo = getResult.CapacityNumSub1F;

        // 正常終了の場合、0を返す
        return seijo;
    }

    /// <summary>
    /// バス指定コード変更に伴い、予約者の移動を行う
    /// </summary>
    /// <param name="zasekiData"></param>
    /// <param name="zasekiImageData"></param>
    /// <param name="zasekiImagemotoData"></param>
    /// <returns>リザルトコード</returns>
    public int getChangeGousya(DataTable zasekiData, DataTable zasekiMotoData, DataTable zasekiImageData, DataTable zasekiImageMotoData)
    {
        int secondFloor = 2;                                  // 2階
        string yoyakuFlgOn = "1";                                 // 予約有
        string eiBlockFlgOn = "1";                                // 営業所ブロック有
        string whereStringKai = "ZASEKI_KAI=" + secondFloor;      // 2階建て判定条件
        string JoseisekiFlg = "1";                                // 女性席
        bool secondFloorFlg = false;                           // 2階建てフラグ
        bool MoveChkFlg = false;                               // 移動チェックフラグ
        string whereStringMoto = string.Empty;                    // 予約席抽出条件（元データ）
        string whereStringSaki = string.Empty;                    // 移動先座席抽出条件
        string whereStringRedies = "LADIES_SEAT_FLG=" + JoseisekiFlg;  // 女性席抽出条件
        var changeSakiData = new DataTable();                             // 変更後移動先データ
        int colOne = 1;                                       // A列
        int colTwo = 2;                                       // B列
        int colThree = 3;                                     // C列
        int colFour = 4;                                      // D列
        int colFive = 5;                                      // E列
        int resultBusTypeDifferent = 10;                      // リザルトコード（車種違い）
        int resultMoveSakiNothing = 20;                       // リザルトコード（移動先座席なし・使用不可）
        int resultNotRediesSeat = 30;                         // リザルトコード（隣が女性席ではない）
        int resultNothingZaseki = 40;                         // リザルトコード（空席なし）
        int resultOk = 0;                                     // リザルトコード（正常終了）
        DataRow[] selectDataMoto;                                 // 予約座席元データ
        DataRow[] selectDataSaki;                                 // 予約座席先データ
        DataRow[] rediesDataMoto;                                 // 女性席元データ
        DataRow[] rediesDataSaki;                                 // 女性席先データ
        DataRow[] chkRediesData;                                  // 女性席チェックデータ

        // 2階建てなのかチェックを行う
        // 移動元が2階建てであるかどうか
        if (zasekiImageMotoData.Select(whereStringKai).Length > 0)
        {
            // 移動先が2階建てであるかどうか
            if (zasekiImageData.Select(whereStringKai).Length > 0)
            {
            }
            // 2階建てフラグをオン
            // secondFloorFlg = True
            else
            {
                // 移動先が2階建て出ない場合、車種違いでエラーを返す
                moveGosyaResult = resultBusTypeDifferent;
                return resultBusTypeDifferent;
            }
        }
        // 移動元が2階建てでない場合に移動先が2階建てであるかどうか
        else if (zasekiImageData.Select(whereStringKai).Length > 0)
        {
            // 移動元と移動先の階数が異なる場合、エラーを返す
            moveGosyaResult = resultBusTypeDifferent;
            return resultBusTypeDifferent;
        }

        // 車種コードチェック
        foreach (DataRow row in zasekiMotoData.Rows)
        {
            foreach (DataRow row2 in zasekiData.Rows)
            {
                // 車種が異なるかどうか
                if (row("CAR_TYPE_CD").ToString == row2("CAR_TYPE_CD").ToString)
                {
                }
                else
                {
                    // 異なる場合、エラーを返す
                    moveGosyaResult = resultBusTypeDifferent;
                    return resultBusTypeDifferent;
                }
            }
        }

        // 座席移動チェック
        // 移動元抽出条件作成（予約フラグ＝１）
        whereStringMoto += "YOYAKU_FLG" + " = " + yoyakuFlgOn;

        // 問合せ対象データ取得
        selectDataMoto = zasekiImageMotoData.Select(whereStringMoto);
        for (int rowMoto = 0, loopTo = selectDataMoto.Length - 1; rowMoto <= loopTo; rowMoto++)
        {
            // 移動先データの抽出条件作成
            // 2階建てである場合
            // If secondFloorFlg = True Then
            // whereStringSaki = "ZASEKI_KAI" & " = " & secondFloor
            // Else
            whereStringSaki = "ZASEKI_KAI" + " = " + selectDataMoto[rowMoto]("ZASEKI_KAI");
            // End If

            whereStringSaki += " AND ";
            whereStringSaki += "ZASEKI_LINE" + " = " + selectDataMoto[rowMoto]("ZASEKI_LINE");
            whereStringSaki += " AND ";
            whereStringSaki += "ZASEKI_COL" + " = " + selectDataMoto[rowMoto]("ZASEKI_COL");

            // 問合せ対象データ取得
            selectDataSaki = zasekiImageData.Select(whereStringSaki);

            // 同一席が存在しない場合
            if (selectDataSaki.Length == 0)
            {
                // 移動先なしのエラーを返す
                moveGosyaResult = resultMoveSakiNothing;
                return resultMoveSakiNothing;
            }
            // 対象席が使用不可の場合
            else if (selectDataSaki[0]("ZASEKI_KIND") == FixedCd.ZasekiKind.notUseSeat)
            {
                // 移動先なしのエラーを返す
                moveGosyaResult = resultMoveSakiNothing;
                return resultMoveSakiNothing;
            }
            // 移動先が既に予約されていた場合
            else if ((selectDataSaki[0]("YOYAKU_FLG") ?? "") == (yoyakuFlgOn ?? ""))
            {
                // 移動先なしのエラーを返す
                moveGosyaResult = resultMoveSakiNothing;
                return resultMoveSakiNothing;
            }
            // 移動先にブロックが設定されていた場合
            else if ((selectDataSaki[0]("EIGYOSYO_BLOCK_KBN") ?? "") == (eiBlockFlgOn ?? ""))
            {
                // 移動先なしのエラーを返す
                moveGosyaResult = resultMoveSakiNothing;
                return resultMoveSakiNothing;
            }
            else
            {
                // 移動チェックフラグをオンにする
                MoveChkFlg = true;
            }
        }

        // 女性専用席チェック
        // 女性専用フラグがたっている座席を抽出
        rediesDataMoto = zasekiImageMotoData.Select(whereStringRedies);
        rediesDataSaki = zasekiImageData.Select(whereStringRedies);

        // 移動元をベースチェック
        for (int rowMoto = 0, loopTo1 = rediesDataMoto.Length - 1; rowMoto <= loopTo1; rowMoto++)
        {
            int sakiCol = 0;
            // 補助席の場合、チェックは行わない
            if (rediesDataMoto[rowMoto]("ZASEKI_COL") == colThree)
            {
            }
            else
            {
                // 元の座席位置に応じて隣席を算出
                // 1の場合：2   2の場合：1   4の場合：5   5の場合：4
                if (rediesDataMoto[rowMoto]("ZASEKI_COL") == colOne)
                {
                    sakiCol = colTwo;
                }
                else if (rediesDataMoto[rowMoto]("ZASEKI_COL") == colOne)
                {
                    sakiCol = colOne;
                }
                else if (rediesDataMoto[rowMoto]("ZASEKI_COL") == colFour)
                {
                    sakiCol = colFive;
                }
                else if (rediesDataMoto[rowMoto]("ZASEKI_COL") == colFive)
                {
                    sakiCol = colFour;
                }

                whereStringSaki = "ZASEKI_KAI" + " = " + rediesDataMoto[rowMoto]("ZASEKI_KAI");
                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_LINE" + " = " + rediesDataMoto[rowMoto]("ZASEKI_LINE");
                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_COL" + " = " + sakiCol;

                // 問合せ対象データ取得
                chkRediesData = zasekiImageData.Select(whereStringSaki);

                // 
                if (chkRediesData.Length == 0)
                {
                }
                // 移動先に予約が入っていて、女性専用席フラグがたっているかチェック
                else if (chkRediesData[0]("YOYAKU_FLG").ToString == yoyakuFlgOn && chkRediesData[0]("LADIES_SEAT_FLG").ToString != JoseisekiFlg)
                {
                    // 隣席に女性席ではない予約が入っていた場合、エラーを返す
                    MoveChkFlg = false;
                    moveGosyaResult = resultNotRediesSeat;
                    return resultNotRediesSeat;
                }
            }
        }

        // 移動先をベースにチェック
        for (int rowMoto = 0, loopTo2 = rediesDataSaki.Length - 1; rowMoto <= loopTo2; rowMoto++)
        {
            int sakiCol = 0;
            // 補助席の場合、チェックは行わない
            if (rediesDataSaki[rowMoto]("ZASEKI_COL") == colThree)
            {
            }
            else
            {
                // 元の座席位置に応じて隣席を算出
                // 1の場合：2   2の場合：1   4の場合：5   5の場合：4
                if (rediesDataSaki[rowMoto]("ZASEKI_COL") == colOne)
                {
                    sakiCol = colTwo;
                }
                else if (rediesDataSaki[rowMoto]("ZASEKI_COL") == colOne)
                {
                    sakiCol = colOne;
                }
                else if (rediesDataSaki[rowMoto]("ZASEKI_COL") == colFour)
                {
                    sakiCol = colFive;
                }
                else if (rediesDataSaki[rowMoto]("ZASEKI_COL") == colFive)
                {
                    sakiCol = colFour;
                }

                whereStringSaki = "ZASEKI_KAI" + " = " + rediesDataSaki[rowMoto]("ZASEKI_KAI");
                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_LINE" + " = " + rediesDataSaki[rowMoto]("ZASEKI_LINE");
                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_COL" + " = " + sakiCol;

                // 問合せ対象データ取得
                chkRediesData = zasekiImageMotoData.Select(whereStringSaki);

                // 
                if (chkRediesData.Length == 0)
                {
                }
                // 移動先に予約が入っていて、女性専用席フラグがたっているかチェック
                else if (chkRediesData[0]("YOYAKU_FLG").ToString == yoyakuFlgOn && chkRediesData[0]("LADIES_SEAT_FLG").ToString != JoseisekiFlg)
                {
                    // 隣席に女性席ではない予約が入っていた場合、エラーを返す
                    MoveChkFlg = false;
                    moveGosyaResult = resultNotRediesSeat;
                    return resultNotRediesSeat;
                }
            }
        }

        // チェックＯＫの場合、移動開始
        if (MoveChkFlg == true)
        {
            for (int rowMoto = 0, loopTo3 = selectDataMoto.Length - 1; rowMoto <= loopTo3; rowMoto++)
            {
                // 移動先データの抽出条件作成
                // 2階建てである場合
                // If secondFloorFlg = True Then
                // whereStringSaki = "ZASEKI_KAI" & " = " & secondFloor
                // Else
                whereStringSaki = "ZASEKI_KAI" + " = " + selectDataMoto[rowMoto]("ZASEKI_KAI");
                // End If

                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_LINE" + " = " + selectDataMoto[rowMoto]("ZASEKI_LINE");
                whereStringSaki += " AND ";
                whereStringSaki += "ZASEKI_COL" + " = " + selectDataMoto[rowMoto]("ZASEKI_COL");

                // 問合せ対象データ取得
                selectDataSaki = zasekiImageData.Select(whereStringSaki);

                // 移動先に移動元の情報を反映
                selectDataSaki[0]("ZASEKI_KBN") = selectDataMoto[rowMoto]("ZASEKI_KBN");                   // 座席区分
                selectDataSaki[0]("YOYAKU_FLG") = selectDataMoto[rowMoto]("YOYAKU_FLG");                   // 予約フラグ
                if (object.ReferenceEquals(selectDataMoto[rowMoto]("YOYAKU_STATUS"), DBNull.Value))
                {
                    selectDataSaki[0]("YOYAKU_STATUS") = string.Empty;
                }
                else
                {
                    selectDataSaki[0]("YOYAKU_STATUS") = selectDataMoto[rowMoto]("YOYAKU_STATUS");
                }         // 予約状態

                selectDataSaki[0]("GROUP_NO") = selectDataMoto[rowMoto]("GROUP_NO");                       // 予約グループNo
                selectDataSaki[0]("ZASEKI_STATE") = selectDataMoto[rowMoto]("ZASEKI_STATE");               // 座席状態
                if (object.ReferenceEquals(selectDataMoto[rowMoto]("YOYAKU_KBN"), DBNull.Value))
                {
                    selectDataSaki[0]("YOYAKU_KBN") = string.Empty;
                }
                else
                {
                    selectDataSaki[0]("YOYAKU_KBN") = selectDataMoto[rowMoto]("YOYAKU_KBN");
                }               // 予約区分

                selectDataSaki[0]("YOYAKU_NO") = selectDataMoto[rowMoto]("YOYAKU_NO");                     // 予約番号
                selectDataSaki[0]("NLGSQN") = selectDataMoto[rowMoto]("NLGSQN");                           // 連番
                selectDataSaki[0]("LADIES_SEAT_FLG") = selectDataMoto[rowMoto]("LADIES_SEAT_FLG");         // 女性専用席フラグ
                selectDataSaki[0]("DELETE_DAY") = selectDataMoto[rowMoto]("DELETE_DAY");                   // 削除日

                // 移動元の予約情報を初期化
                selectDataMoto[0]("ZASEKI_KBN") = "0";                 // 座席区分
                selectDataMoto[0]("YOYAKU_FLG") = "0";                 // 予約フラグ
                selectDataMoto[0]("YOYAKU_STATUS") = string.Empty;     // 予約状態
                selectDataMoto[0]("GROUP_NO") = 0;                     // 予約グループNo
                selectDataMoto[0]("ZASEKI_STATE") = 0;                 // 座席状態
                selectDataMoto[0]("YOYAKU_KBN") = string.Empty;        // 予約区分
                selectDataMoto[0]("YOYAKU_NO") = 0;                    // 予約番号
                selectDataMoto[0]("NLGSQN") = 0;                       // 連番
                selectDataMoto[0]("LADIES_SEAT_FLG") = "0";            // 女性専用席フラグ
                selectDataMoto[0]("DELETE_DAY") = 0;                   // 削除日

                // 変更した移動先データを変更データとして保持
                changeSakiData = zasekiImageData.Clone();
                changeSakiData.ImportRow(selectDataSaki[0]);
            }

            // 変更内容をパラメータの移動元・移動先の座席データにデータを反映する
            // 移動元
            foreach (DataRow row in zasekiImageMotoData.Rows)
            {
                for (int rowMoto = 0, loopTo4 = selectDataMoto.Length - 1; rowMoto <= loopTo4; rowMoto++)
                {
                    if (row("ZASEKI_KAI").ToString == selectDataMoto[rowMoto]("ZASEKI_KAI").ToString && row("ZASEKI_LINE").ToString == selectDataMoto[rowMoto]("ZASEKI_LINE").ToString && row("ZASEKI_COL").ToString == selectDataMoto[rowMoto]("ZASEKI_COL").ToString)
                    {

                        // 移動先に移動元の情報を反映
                        if (object.ReferenceEquals(selectDataMoto[rowMoto]("ZASEKI_KBN"), DBNull.Value))
                        {
                            row("ZASEKI_KBN") = "0";
                        }
                        else
                        {
                        }

                        row("ZASEKI_KBN") = selectDataMoto[rowMoto]("ZASEKI_KBN");                   // 座席区分
                        row("YOYAKU_FLG") = selectDataMoto[rowMoto]("YOYAKU_FLG");                   // 予約フラグ
                        row("YOYAKU_STATUS") = selectDataMoto[rowMoto]("YOYAKU_STATUS");                   // 予約状態
                        row("GROUP_NO") = selectDataMoto[rowMoto]("GROUP_NO");                       // 予約グループNo
                        row("ZASEKI_STATE") = selectDataMoto[rowMoto]("ZASEKI_STATE");               // 座席状態
                        row("YOYAKU_KBN") = selectDataMoto[rowMoto]("YOYAKU_KBN");                         // 予約区分
                        row("YOYAKU_NO") = selectDataMoto[rowMoto]("YOYAKU_NO");                     // 予約番号
                        row("NLGSQN") = selectDataMoto[rowMoto]("NLGSQN");                           // 連番
                        row("LADIES_SEAT_FLG") = selectDataMoto[rowMoto]("LADIES_SEAT_FLG");         // 女性専用席フラグ
                        row("DELETE_DAY") = selectDataMoto[rowMoto]("DELETE_DAY");                   // 削除日
                    }
                }
            }

            // 移動先
            foreach (DataRow row in zasekiImageData.Rows)
            {
                foreach (DataRow row2 in changeSakiData.Rows)
                {
                    if (row("ZASEKI_KAI").ToString == row2("ZASEKI_KAI").ToString && row("ZASEKI_LINE").ToString == row2("ZASEKI_LINE").ToString && row("ZASEKI_COL").ToString == row2("ZASEKI_COL").ToString)
                    {

                        // 移動先に移動元の情報を反映
                        row("ZASEKI_KBN") = row2("ZASEKI_KBN");                   // 座席区分
                        row("YOYAKU_FLG") = row2("YOYAKU_FLG");                   // 予約フラグ
                        row("YOYAKU_STATUS") = row2("YOYAKU_STATUS");                   // 予約状態
                        row("GROUP_NO") = row2("GROUP_NO");                       // 予約グループNo
                        row("ZASEKI_STATE") = row2("ZASEKI_STATE");               // 座席状態
                        row("YOYAKU_KBN") = row2("YOYAKU_KBN");                         // 予約区分
                        row("YOYAKU_NO") = row2("YOYAKU_NO");                     // 予約番号
                        row("NLGSQN") = row2("NLGSQN");                           // 連番
                        row("LADIES_SEAT_FLG") = row2("LADIES_SEAT_FLG");         // 女性専用席フラグ
                        row("DELETE_DAY") = row2("DELETE_DAY");                   // 削除日
                    }
                }
            }

            // 空席再計算処理を呼び出す
            if (getKusekiNum(zasekiImageData, zasekiData) == false)
            {
                moveGosyaResult = resultNothingZaseki;
                return resultNothingZaseki;
            }
            else
            {
                zasekiData[0]("KUSEKI_NUM_TEISEKI") = kusekiRegular;
                zasekiData[0]("KUSEKI_NUM_SUB_SEAT") = kusekiHo;
            }

            zasekiBusInfoData = zasekiData.Copy;
            zasekiInfoData = zasekiImageData.Copy;
        }

        // 正常終了の場合、0を返す
        moveGosyaResult = resultOk;
        return resultOk;
    }

    /// <summary>
    /// バス単位用メソッド（後に共通化）
    /// </summary>
    public void setBusTaniCheckBox(string supplierKind, ref CheckBoxEx busCheckBox)
    {
        // チェックボックスを初期化
        busCheckBox.Visible = true;
        busCheckBox.Enabled = true;
        if (SuppliersKind_Carrier.Equals(supplierKind))
        {
            // ・80：キャリア
            // ⇒原価設定がないため対象外
            busCheckBox.Checked = false;
            busCheckBox.Visible = false;
        }
        else if (SuppliersKind_Kosoku_TollRoad.Equals(supplierKind))
        {
            // ・50：高速_有料道路
            // ⇒バス単位のみ設定可
            busCheckBox.Checked = true;
            busCheckBox.Enabled = false;
            busCheckBox.Enabled = false;
        }
        else if (SuppliersKind_Stay.Equals(supplierKind) | SuppliersKind_Hotel_Coupon.Equals(supplierKind) | SuppliersKind_StayPlan.Equals(supplierKind))

        {
            // ・30：宿泊
            // ・35：ホテルクーポン
            // ・45：宿泊プラン　←ここだけ修正しました
            // ⇒人員区分のみ設定可
            busCheckBox.Checked = false;
            busCheckBox.Enabled = false;
        }
        else if (SuppliersKind_Koshakasho.Equals(supplierKind) | SuppliersKind_SonotaExpense.Equals(supplierKind) | SuppliersKind_SonotaKakuteiExpense.Equals(supplierKind))

        {
            // ・40：降車ヶ所
            // ・90：その他経費
            // ・99：その他確定経費
            // ⇒人員区分・バス単位両方設定可
            // 制御無し
        }
    }

    #endregion

}