﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic; // Install-Package Microsoft.VisualBasic
using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic

internal static partial class TehaiDebug
{
    #region Entity->Csv(デバッグ用)
    public static bool outPutEntity(EntityOperation crsMst, string addName = "")
    {
        // 実行フォルダ
        string baseDir = Environment.CurrentDirectory;
        string fullPath = string.Empty;
        // 時間のフォルダを作成する
        baseDir = Path.Combine(baseDir, DateTime.Now.ToString("yyyyMMddHHmmss") + addName);
        Directory.CreateDirectory(baseDir);
        EntityOperation a;
        DataTable dt;
        // コース台帳(基本)
        dt = entityToDataTable(crsMst);
        fullPath = Path.Combine(baseDir, "コース台帳_基本.csv");
        outputDataTableToCSV(dt, fullPath);

        // コース台帳（基本_料金区分）
        dt = entityToDataTable(crsMst.EntityData(0).ChargeKbnEntity);
        fullPath = Path.Combine(baseDir, "コース台帳_料金区分.csv");
        outputDataTableToCSV(dt, fullPath);

        // コース台帳原価（プレート）
        dt = entityToDataTable(crsMst.EntityData(0).CostPlateEntity);
        fullPath = Path.Combine(baseDir, "コース台帳_プレート.csv");
        outputDataTableToCSV(dt, fullPath);

        // コース台帳原価（基本）
        dt = entityToDataTable(crsMst.EntityData(0).CostBasicEntity);
        fullPath = Path.Combine(baseDir, "原価_基本.csv");
        outputDataTableToCSV(dt, fullPath);
        baseDir = Path.Combine(baseDir, "降車ヶ所");
        Directory.CreateDirectory(baseDir);

        // 降車ヶ所
        dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity);
        fullPath = Path.Combine(baseDir, "コース台帳_降車ヶ所.csv");
        outputDataTableToCSV(dt, fullPath);
        for (int i = 0, loopTo = crsMst.EntityData(0).KoushakashoEntity.EntityData.Length - 1; i <= loopTo; i++)
        {
            Directory.CreateDirectory(Path.Combine(baseDir, i.ToString()));

            // 'コース台帳（ホテル）
            // dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity(i).HotelEntity)
            // fullPath = IO.Path.Combine(Path.Combine(baseDir, i.ToString), "原価_ホテル.csv")
            // outputDataTableToCSV(dt, fullPath)


            // コース台帳原価（降車ヶ所）
            dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity(i).CostKoshakashoEntity);
            fullPath = Path.Combine(Path.Combine(baseDir, i.ToString()), "原価_降車ヶ所.csv");
            outputDataTableToCSV(dt, fullPath);

            // コース台帳原価（降車ヶ所_料金区分）
            for (int j = 0, loopTo1 = crsMst.EntityData(0).KoushakashoEntity(i).CostKoshakashoEntity.EntityData.Length - 1; j <= loopTo1; j++)
            {
                dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity(i).CostKoshakashoEntity(j).CostKoshakashoChargeKbnEntity);
                // fullPath = IO.Path.Combine(Path.Combine(baseDir, i.ToString), "原価_降車ヶ所_料金区分_" & j.ToString() & ".csv")
                fullPath = Path.Combine(Path.Combine(baseDir, i.ToString()), "原価_降車ヶ所_料金区分.csv");
                outputDataTableToCSV(dt, fullPath);
            }

            // コース台帳原価（キャリア）
            dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity(i).CostCarrierEntity);
            fullPath = Path.Combine(Path.Combine(baseDir, i.ToString()), "原価_キャリア.csv");
            outputDataTableToCSV(dt, fullPath);

            // コース台帳原価（キャリア_料金区分）
            for (int j = 0, loopTo2 = crsMst.EntityData(0).KoushakashoEntity(i).CostCarrierEntity.EntityData.Length - 1; j <= loopTo2; j++)
            {
                dt = entityToDataTable(crsMst.EntityData(0).KoushakashoEntity(i).CostCarrierEntity(j).CostCarrierChargeKbnEntity);
                // fullPath = IO.Path.Combine(Path.Combine(baseDir, i.ToString), "原価_キャリア_料金区分_" & j.ToString() & ".csv")
                fullPath = Path.Combine(Path.Combine(baseDir, i.ToString()), "原価_キャリア_料金区分.csv");
                outputDataTableToCSV(dt, fullPath);
            }
        }

        return true;
    }

    private static void outputDataTableToCSV(DataTable dt, string outputPath)
    {
        // ヘッダー
        var strString = new StringBuilder();
        for (int i = 0, loopTo = dt.Columns.Count - 1; i <= loopTo; i++)
            strString.Append(dt.Columns(i).ColumnName).Append(ControlChars.Tab);
        strString.Remove(strString.ToString().Length - 1, 1);
        strString.AppendLine("");
        for (int j = 0, loopTo1 = dt.Rows.Count - 1; j <= loopTo1; j++)
            strString.AppendLine(string.Join(ControlChars.Tab, dt.Rows(j).ItemArray.ToArray));
        var enc = Encoding.GetEncoding("utf-8");
        // TextBox1の内容を書き込む 
        // ファイルが存在しているときは、上書きする 
        File.WriteAllText(outputPath, strString.ToString(), enc);
    }

    /// <summary>
    /// エンティティデータを データテーブル に変換
    /// </summary>
    /// <param name="headEnt"></param>
    /// <returns></returns>
    public static DataTable entityToDataTable(object headEnt)
    {
        var dtHead = new DataTable("ENTITYTABLE");
        DataRow drHead;

        // カラム情報作成
        dtHead.Columns.Add("ROW_NUM").AutoIncrement = true;
        for (int idx = 0, loopTo = Conversions.ToInteger(Operators.SubtractObject(headEnt.getPropertyDataLength, 1)); idx <= loopTo; idx++)
            dtHead.Columns.Add(headEnt.getPtyValue(idx, headEnt.EntityData(0)).PhysicsName);
        // データを設定
        for (int idxHead = 0, loopTo1 = Conversions.ToInteger(Operators.SubtractObject(headEnt.EntityData.Length, 1)); idxHead <= loopTo1; idxHead++)
        {
            drHead = dtHead.Rows.Add();
            for (int idxCols = 0, loopTo2 = Conversions.ToInteger(Operators.SubtractObject(headEnt.getPropertyDataLength, 1)); idxCols <= loopTo2; idxCols++)
                drHead.Item(Conversions.ToString(headEnt.getPtyValue(idxCols, headEnt.EntityData(idxHead)).PhysicsName)) = headEnt.getPtyValue(idxCols, headEnt.EntityData(idxHead)).value;
        }

        return dtHead;
    }

    #endregion
}