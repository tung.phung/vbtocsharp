﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 運行バス会社決定通知パラメータクラス
	/// </summary>
	public class P03_0501ParamData
	{

		/// <summary>
		/// バス会社確定通知/履歴照会
		/// </summary>
		/// <returns></returns>
		public DataTable BusNoticeDt {get; set;}

		/// <summary>
		/// バス会社確定通知/履歴照会(サブレポート1)
		/// </summary>
		/// <returns></returns>
		public DataTable BusNoticeSubDt {get; set;}

		/// <summary>
		/// 送付元情報テーブル
		/// </summary>
		/// <returns></returns>
		public DataTable SenderInfomationDt {get; set;}

		/// <summary>
		/// 案内文用テーブル
		/// </summary>
		/// <returns></returns>
		public DataTable GuideDt {get; set;}

		/// <summary>
		/// 注意事項用テーブル
		/// </summary>
		/// <returns></returns>
		public DataTable PrecautionsDt {get; set;}
	}

}