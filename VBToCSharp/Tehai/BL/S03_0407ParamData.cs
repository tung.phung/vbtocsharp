﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 催行決定・中止連絡履歴DA
	///（画面間受渡しパラメータ)
	/// </summary>
	public class S03_0407ParamData
	{
		/// <summary>
		/// 出発日FROM
		/// </summary>
		public int? SyuptDayFrom {get; set;}
		/// <summary>
		/// 出発日TO
		/// </summary> 
		public int? SyuptDayTo {get; set;}
		/// <summary>
		/// コースコード
		/// </summary>
		public string CrsCd {get; set;}
		/// <summary>
		/// コース名
		/// </summary>
		public string CrsName {get; set;}
	}

}