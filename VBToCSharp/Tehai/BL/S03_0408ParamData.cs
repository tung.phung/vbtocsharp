﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 通知先設定パラメータクラス
	/// </summary>
	public class S03_0408ParamData
	{

		/// <summary>
		/// 通知種別
		/// </summary>
		public string NoticeType {get; set;}
		/// <summary>
		/// 通知先
		/// </summary>
		public List<string> Notice {get; set;} = new List<string>();
		/// <summary>
		/// 受け取るパラメータクラスのリスト
		/// </summary>
		public List<S03_0408ParamSubData> paramList {get; set;} = new List<S03_0408ParamSubData>();
	}

}