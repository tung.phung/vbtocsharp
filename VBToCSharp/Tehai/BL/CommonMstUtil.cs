﻿/// <summary>
/// マスタメンテナンス共通処理クラス
/// </summary>
using System.Linq;

public partial class CommonMstUtil
{

    /// <summary>
    /// WHERE文字列作成
    /// </summary>
    /// <param name="grd">対象Grid</param>
    /// <param name="keys">Key配列</param>
    public static string MakeWhere(FlexGridEx grd, string[] keys)
    {
        string whereString = "";
        if (grd.Row >= 0 & grd.Rows.Count > 1)
        {
            foreach (string s in keys)
            {
                if (grd.Item(grd.Row, s).ToString != string.Empty)
                {
                    whereString += s + " = '" + grd.Item(grd.Row, s).ToString + "'";
                }
            }
        }

        return whereString;
    }

    /// <summary>
    /// コンボの共通的なプロパティ設定
    /// </summary>
    /// <param name="cmbParam"></param>
    /// <param name="dataSourceParram"></param>
    public static void setComboCommonProperty(ComboBoxEx cmbParam, DataTable dataSourceParram)
    {
        cmbParam.DataSource = dataSourceParram;
        cmbParam.DropDown.AllowResize = false;
        cmbParam.ListHeaderPane.Visible = false;
        cmbParam.ListColumns(ComboBoxCdType.CODE_VALUE).Visible = false;
        cmbParam.ListColumns(ComboBoxCdType.CODE_NAME).Width = cmbParam.Width;
        cmbParam.TextSubItemIndex = ComboBoxCdType.CODE_NAME;
    }
}