﻿/// <summary>
/// 固定コード
/// </summary>
/// <remarks></remarks>

public static partial class FixedCdTehai
{

    #region  固定コード 

    /// <summary>
    /// 運休区分
    /// </summary>
    /// <remarks></remarks>
    public sealed partial class UnkyuKbn
    {
        [Value("運休")]
        public const string Unkyu = "Y";
        [Value("廃止")]
        public const string Haishi = "X";
    }

    /// <summary>
    /// 催行確定区分
    /// </summary>
    /// <remarks></remarks>
    public sealed partial class SaikouKakuteiKbn
    {
        [Value("催行")]
        public const string Saikou = "Y";
        [Value("中止")]
        public const string Tyushi = "N";
        [Value("廃止")]
        public const string Haishi = "X";
    }

    /// <summary>
    /// <summary>
    /// コース種別 
    /// </summary>
    /// <remarks></remarks>
    public enum CourseType : int
    {
        [Value("定期")]
        teiki = 1,
        [Value("キャピタル")]
        capital = 2,
        [Value("企画（日帰り）")]
        higaeri = 4,
        [Value("企画（宿泊）")]
        syukuhaku = 5,
        [Value("企画（Rコース）")]
        rcourse = 6
    }
    /// <summary>
    /// コース区分
    /// </summary>
    /// <remarks></remarks>
    public enum CourseKbn : int
    {
        [Value("定期（昼）")]
        teikiHiru = 1,
        [Value("定期（夜）")]
        teikiYoru = 2
    }
    /// 使用中フラグ
    /// </summary>
    /// <remarks></remarks>
    public sealed partial class UsingFlg
    {
        [Value("使用中")]
        public const string Use = "Y";
        [Value("未使用")]
        public const string Unused = null;
    }

    #endregion

}