﻿
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 催行決定・中止連絡パラメータクラス
	/// </summary>
	public class P03_0406ParamData
	{

		/// <summary>
		/// 通知先連絡データ
		/// </summary>
		/// <returns></returns>
		public DataTable NoticeDt {get; set;}

		/// <summary>
		/// 送付元情報テーブル
		/// </summary>
		/// <returns></returns>
		public DataTable SenderInfomationDt {get; set;}
	}

}