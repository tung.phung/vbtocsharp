﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 通知先設定受け取るパラメータ
	/// リストで受け取るため
	/// </summary>
	public class S03_0408ParamSubData
	{
		/// <summary>
		/// 出発日
		/// </summary>
		public int SyuptDay {get; set;}
		/// <summary>
		/// コースコード
		/// </summary>
		public string CrsCd {get; set;}
		/// <summary>
		/// 号車
		/// </summary>
		public int? Gousya {get; set;}
	}

}