﻿//INSTANT C# NOTE: Formerly VB project-level imports:
using Hatobus.ReservationManagementSystem.ClientCommon;
using Hatobus.ReservationManagementSystem.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Hatobus.ReservationManagementSystem.Tehai
{
	/// <summary>
	/// 手仕舞い情報登録パラメータクラス
	/// </summary>
	public class TejimaiParamData
	{
		/// <summary>
		/// かがみ（チェック有無）
		/// </summary>
		/// <returns></returns>
		public string KagamiFlg {get; set;}

		/// <summary>
		/// 出発日
		/// </summary>
		/// <returns></returns>
		public string SyuptDayStr {get; set;}

		/// <summary>
		/// コースコード
		/// </summary>
		/// <returns></returns>
		public string CrsCd {get; set;}

		/// <summary>
		/// 仕入先コード
		/// </summary>
		/// <returns></returns>
		public string SiireSakiCd {get; set;}

		/// <summary>
		/// 仕入先枝番
		/// </summary>
		/// <returns></returns>
		public string SiireSakiNo {get; set;}

		/// <summary>
		/// 一室最大人数
		/// </summary>
		/// <returns></returns>
		public string RoomMaxCapacity {get; set;}

	}

}