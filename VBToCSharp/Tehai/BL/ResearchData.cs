﻿/// <summary>
/// プロパティ受渡し
/// </summary>
using System;

public partial class ResearchData
{
    private DateTime DepartureDayFrom_R = default;
    private DateTime DepartureDayTo_R = default;
    private string CrsCd_Hedder_R = string.Empty;
    private string CrsName_Hedder_R = string.Empty;
    private DataTable Prmtable_R = default;
    private DataTable prmtable_R2 = default;
    private DataTable prmtable_R3 = default;
    private Teiki_KikakuKbnType TeikiKikakuKbn_R;
    private CrsKindType CoruseSyuBetsu_R;
    private SyuptJiCarrierKbnType SyuptjiCarrier_R;

    public DateTime DepartureDayFrom
    {
        get
        {
            return DepartureDayFrom_R;
        }

        set
        {
            DepartureDayFrom_R = value;
        }
    }

    public DateTime DepartureDayTo
    {
        get
        {
            return DepartureDayTo_R;
        }

        set
        {
            DepartureDayTo_R = value;
        }
    }

    public string CrsCd_Hedder
    {
        get
        {
            return CrsCd_Hedder_R;
        }

        set
        {
            CrsCd_Hedder_R = value;
        }
    }

    public string CrsName_Hedder
    {
        get
        {
            return CrsName_Hedder_R;
        }

        set
        {
            CrsName_Hedder_R = value;
        }
    }

    public Teiki_KikakuKbnType TeikiKikakuKbn
    {
        get
        {
            return TeikiKikakuKbn_R;
        }

        set
        {
            TeikiKikakuKbn_R = value;
        }
    }

    public CrsKindType CoruseSyuBetsu
    {
        get
        {
            return CoruseSyuBetsu_R;
        }

        set
        {
            CoruseSyuBetsu_R = value;
        }
    }

    public SyuptJiCarrierKbnType SyuptjiCarrier
    {
        get
        {
            return SyuptjiCarrier_R;
        }

        set
        {
            SyuptjiCarrier_R = value;
        }
    }

    public DataTable Prmtable
    {
        get
        {
            return Prmtable_R;
        }

        set
        {
            Prmtable_R = value;
        }
    }

    public DataTable Prmtable_Hattyaku
    {
        get
        {
            return prmtable_R2;
        }

        set
        {
            prmtable_R2 = value;
        }
    }

    public DataTable Prmtable_zaseki
    {
        get
        {
            return prmtable_R3;
        }

        set
        {
            prmtable_R3 = value;
        }
    }
}