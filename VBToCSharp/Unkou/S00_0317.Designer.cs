﻿
using Unkou.Common;

namespace Unkou
{
    partial class S00_0317
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S00_0317));
			this.btnMihakkenYoyakuListInquiry = new ButtonEx();
			this.btnNoShowList = new ButtonEx();
			this.btnCustomerSituationListInquiry = new ButtonEx();
			this.pnlBaseFill.SuspendLayout();
			this.SuspendLayout();
			//
			//pnlBaseFill
			//
			this.pnlBaseFill.Controls.Add(this.btnCustomerSituationListInquiry);
			this.pnlBaseFill.Controls.Add(this.btnNoShowList);
			this.pnlBaseFill.Controls.Add(this.btnMihakkenYoyakuListInquiry);
			//
			//btnMihakkenYoyakuListInquiry
			//
			this.btnMihakkenYoyakuListInquiry.AutoSize = true;
			this.btnMihakkenYoyakuListInquiry.BackColor = System.Drawing.Color.Transparent;
			this.btnMihakkenYoyakuListInquiry.BtnEventId = ClientConstantCode.BtnKeyId.Null;
			this.btnMihakkenYoyakuListInquiry.ExistError = false;
			this.btnMihakkenYoyakuListInquiry.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnMihakkenYoyakuListInquiry.ForeColor = System.Drawing.Color.Black;
			this.btnMihakkenYoyakuListInquiry.GuideMessage = "";
			this.btnMihakkenYoyakuListInquiry.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnMihakkenYoyakuListInquiry.GuideMessageDetail"));
			this.btnMihakkenYoyakuListInquiry.Location = new System.Drawing.Point(12, 8);
			this.btnMihakkenYoyakuListInquiry.Name = "btnMihakkenYoyakuListInquiry";
			this.btnMihakkenYoyakuListInquiry.NotNull = false;
			this.btnMihakkenYoyakuListInquiry.Size = new System.Drawing.Size(394, 27);
			this.btnMihakkenYoyakuListInquiry.TabIndex = 1;
			this.btnMihakkenYoyakuListInquiry.Text = "乗車状況照会";
			this.btnMihakkenYoyakuListInquiry.UseVisualStyleBackColor = true;
			//
			//btnNoShowList
			//
			this.btnNoShowList.AutoSize = true;
			this.btnNoShowList.BackColor = System.Drawing.Color.Transparent;
			this.btnNoShowList.BtnEventId = ClientConstantCode.BtnKeyId.Null;
			this.btnNoShowList.ExistError = false;
			this.btnNoShowList.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnNoShowList.ForeColor = System.Drawing.Color.Black;
			this.btnNoShowList.GuideMessage = "";
			this.btnNoShowList.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnNoShowList.GuideMessageDetail"));
			this.btnNoShowList.Location = new System.Drawing.Point(12, 41);
			this.btnNoShowList.Name = "btnNoShowList";
			this.btnNoShowList.NotNull = false;
			this.btnNoShowList.Size = new System.Drawing.Size(394, 27);
			this.btnNoShowList.TabIndex = 2;
			this.btnNoShowList.Text = "NO SHOW 一覧";
			this.btnNoShowList.UseVisualStyleBackColor = true;
			//
			//btnCustomerSituationListInquiry
			//
			this.btnCustomerSituationListInquiry.AutoSize = true;
			this.btnCustomerSituationListInquiry.BackColor = System.Drawing.Color.Transparent;
			this.btnCustomerSituationListInquiry.BtnEventId = ClientConstantCode.BtnKeyId.Null;
			this.btnCustomerSituationListInquiry.ExistError = false;
			this.btnCustomerSituationListInquiry.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnCustomerSituationListInquiry.ForeColor = System.Drawing.Color.Black;
			this.btnCustomerSituationListInquiry.GuideMessage = "";
			this.btnCustomerSituationListInquiry.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnCustomerSituationListInquiry.GuideMessageDetail"));
			this.btnCustomerSituationListInquiry.Location = new System.Drawing.Point(12, 74);
			this.btnCustomerSituationListInquiry.Name = "btnCustomerSituationListInquiry";
			this.btnCustomerSituationListInquiry.NotNull = false;
			this.btnCustomerSituationListInquiry.Size = new System.Drawing.Size(394, 27);
			this.btnCustomerSituationListInquiry.TabIndex = 3;
			this.btnCustomerSituationListInquiry.Text = "お客様状況一覧照会";
			this.btnCustomerSituationListInquiry.UseVisualStyleBackColor = true;
			//
			//S00_0317
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(8.0F, 15.0F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 862);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.F10Key_Visible = true;
			this.F11Key_Visible = true;
			this.F12Key_Visible = true;
			this.F1Key_Visible = true;
			this.F2Key_Visible = true;
			this.F3Key_Visible = true;
			this.F4Key_Visible = true;
			this.F5Key_Visible = true;
			this.F6Key_Visible = true;
			this.F7Key_Visible = true;
			this.F8Key_Visible = true;
			this.F9Key_Visible = true;
			this.Name = "S00_0317";
			this.setTitle = "";
			this.setTitleVisible = true;
			this.setWindowTitle = "S00_0317";
			this.Text = "S00_0317";
			this.pnlBaseFill.ResumeLayout(false);
			this.pnlBaseFill.PerformLayout();
			this.ResumeLayout(false);

		}

		internal ButtonEx btnMihakkenYoyakuListInquiry;
		internal ButtonEx btnNoShowList;
		internal ButtonEx btnCustomerSituationListInquiry;
	}
}