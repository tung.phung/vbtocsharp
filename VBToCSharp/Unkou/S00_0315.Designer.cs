﻿
using Common.UserControl;
using static Unkou.Common.ClientConstantCode;

namespace Unkou
{
    partial class S00_0315
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S00_0315));
			this.btnRiyouNinzuKakuteiRev = new ButtonEx();
			this.btnSensyaKenAＧＴJyosyaKakuninInput = new ButtonEx();
			this.btnUriageKakuteiInquiryTeiki = new ButtonEx();
			this.pnlBaseFill.SuspendLayout();
			this.SuspendLayout();
			//
			//pnlBaseFill
			//
			this.pnlBaseFill.Controls.Add(this.btnUriageKakuteiInquiryTeiki);
			this.pnlBaseFill.Controls.Add(this.btnSensyaKenAＧＴJyosyaKakuninInput);
			this.pnlBaseFill.Controls.Add(this.btnRiyouNinzuKakuteiRev);
			//
			//btnRiyouNinzuKakuteiRev
			//
			this.btnRiyouNinzuKakuteiRev.AutoSize = true;
			this.btnRiyouNinzuKakuteiRev.BackColor = System.Drawing.Color.Transparent;
			this.btnRiyouNinzuKakuteiRev.BtnEventId = BtnKeyId.Null;
			this.btnRiyouNinzuKakuteiRev.ExistError = false;
			this.btnRiyouNinzuKakuteiRev.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnRiyouNinzuKakuteiRev.ForeColor = System.Drawing.Color.Black;
			this.btnRiyouNinzuKakuteiRev.GuideMessage = "";
			this.btnRiyouNinzuKakuteiRev.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnRiyouNinzuKakuteiRev.GuideMessageDetail"));
			this.btnRiyouNinzuKakuteiRev.Location = new System.Drawing.Point(12, 8);
			this.btnRiyouNinzuKakuteiRev.Name = "btnRiyouNinzuKakuteiRev";
			this.btnRiyouNinzuKakuteiRev.NotNull = false;
			this.btnRiyouNinzuKakuteiRev.Size = new System.Drawing.Size(394, 27);
			this.btnRiyouNinzuKakuteiRev.TabIndex = 1;
			this.btnRiyouNinzuKakuteiRev.Text = "利用人員確定・修正";
			this.btnRiyouNinzuKakuteiRev.UseVisualStyleBackColor = true;
			//
			//btnSensyaKenAＧＴJyosyaKakuninInput
			//
			this.btnSensyaKenAＧＴJyosyaKakuninInput.AutoSize = true;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.BackColor = System.Drawing.Color.Transparent;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.BtnEventId = BtnKeyId.Null;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.ExistError = false;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnSensyaKenAＧＴJyosyaKakuninInput.ForeColor = System.Drawing.Color.Black;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.GuideMessage = "";
			this.btnSensyaKenAＧＴJyosyaKakuninInput.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnSensyaKenAＧＴJyosyaKakuninInput.GuideMessageDetail"));
			this.btnSensyaKenAＧＴJyosyaKakuninInput.Location = new System.Drawing.Point(12, 41);
			this.btnSensyaKenAＧＴJyosyaKakuninInput.Name = "btnSensyaKenAＧＴJyosyaKakuninInput";
			this.btnSensyaKenAＧＴJyosyaKakuninInput.NotNull = false;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.Size = new System.Drawing.Size(394, 27);
			this.btnSensyaKenAＧＴJyosyaKakuninInput.TabIndex = 2;
			this.btnSensyaKenAＧＴJyosyaKakuninInput.Text = "船車券ＡＧＴ乗車確認入力";
			this.btnSensyaKenAＧＴJyosyaKakuninInput.UseVisualStyleBackColor = true;
			//
			//btnUriageKakuteiInquiryTeiki
			//
			this.btnUriageKakuteiInquiryTeiki.AutoSize = true;
			this.btnUriageKakuteiInquiryTeiki.BackColor = System.Drawing.Color.Transparent;
			this.btnUriageKakuteiInquiryTeiki.BtnEventId = BtnKeyId.Null;
			this.btnUriageKakuteiInquiryTeiki.ExistError = false;
			this.btnUriageKakuteiInquiryTeiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnUriageKakuteiInquiryTeiki.ForeColor = System.Drawing.Color.Black;
			this.btnUriageKakuteiInquiryTeiki.GuideMessage = "";
			this.btnUriageKakuteiInquiryTeiki.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnUriageKakuteiInquiryTeiki.GuideMessageDetail"));
			this.btnUriageKakuteiInquiryTeiki.Location = new System.Drawing.Point(12, 74);
			this.btnUriageKakuteiInquiryTeiki.Name = "btnUriageKakuteiInquiryTeiki";
			this.btnUriageKakuteiInquiryTeiki.NotNull = false;
			this.btnUriageKakuteiInquiryTeiki.Size = new System.Drawing.Size(394, 27);
			this.btnUriageKakuteiInquiryTeiki.TabIndex = 3;
			this.btnUriageKakuteiInquiryTeiki.Text = "売上確定照会";
			this.btnUriageKakuteiInquiryTeiki.UseVisualStyleBackColor = true;
			//
			//S00_0315
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(8.0F, 15.0F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 862);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.F10Key_Visible = true;
			this.F11Key_Visible = true;
			this.F12Key_Visible = true;
			this.F1Key_Visible = true;
			this.F2Key_Visible = true;
			this.F3Key_Visible = true;
			this.F4Key_Visible = true;
			this.F5Key_Visible = true;
			this.F6Key_Visible = true;
			this.F7Key_Visible = true;
			this.F8Key_Visible = true;
			this.F9Key_Visible = true;
			this.Name = "S00_0315";
			this.setTitle = "";
			this.setTitleVisible = true;
			this.setWindowTitle = "S00_03015";
			this.Text = "S00_03015";
			this.pnlBaseFill.ResumeLayout(false);
			this.pnlBaseFill.PerformLayout();
			this.ResumeLayout(false);

		}

		internal ButtonEx btnRiyouNinzuKakuteiRev;
		internal ButtonEx btnSensyaKenAＧＴJyosyaKakuninInput;
		internal ButtonEx btnUriageKakuteiInquiryTeiki;

		#endregion
	}
}