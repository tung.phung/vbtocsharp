﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unkou
{
	/// <summary>
	/// S00_0317 運行サブメニュー（受付状況確認）
	/// </summary>
	public partial class S00_0317 : FormBase
	{

		#region イベント

		/// <summary>
		/// フォーム起動時の独自処理
		/// </summary>
		protected override void StartupOrgProc()
		{

			//画面上ボタン制御
			this.setBtnControl();

			//画面初期化
			this.setControlInitiarize();

			//フォーカス設定
			this.ActiveControl = this.btnMihakkenYoyakuListInquiry;

		}

		/// <summary>
		/// F2：戻るボタン押下イベント
		/// </summary>
		protected override void btnF2_ClickOrgProc()
		{

			this.Close();
		}

		#endregion

		#region フッタ

		/// <summary>
		/// 乗車状況照会ボタン実行時のイベント
		/// </summary>
		private void btnMihakkenYoyakuListInquiry_Click(object sender, EventArgs e)
		{
			//乗車状況照会へ遷移
			using (S04_0301 form = new S04_0301())
			{
				form.ShowDialog();
			}

		}

		/// <summary>
		/// NO SHOW一覧ボタン実行時のイベント
		/// </summary>
		private void btnNoShowList_Click(object sender, EventArgs e)
		{
			//NO SHOW 一覧へ遷移
			//using (S04_0303 form = new S04_0303())
			//{
			//	form.ShowDialog();
			//}

		}

		/// <summary>
		/// お客様状況一覧照会ボタン実行時のイベント
		/// </summary>
		private void btnCustomerSituationListInquiry_Click(object sender, EventArgs e)
		{
			//お客様状況一覧照会へ遷移
			//Using form As New S04_0304
			//    form.ShowDialog()
			//End Using
		}

		#endregion

		#region メソッド

		/// <summary>
		/// 画面初期化
		/// </summary>
		private void setControlInitiarize()
		{

			//ベースフォームの設定
			this.setFormId = "S00_0317";
			this.setTitle = "運行サブメニュー（受付状況確認）";

			//Visible
			this.F1Key_Visible = false;
			this.F2Key_Visible = true;
			this.F3Key_Visible = false;
			this.F4Key_Visible = false;
			this.F5Key_Visible = false;
			this.F6Key_Visible = false;
			this.F7Key_Visible = false;
			this.F8Key_Visible = false;
			this.F9Key_Visible = false;
			this.F10Key_Visible = false;
			this.F11Key_Visible = false;
			this.F12Key_Visible = false;

			//Text
			this.F2Key_Text = "F2:戻る";

		}

		private void setBtnControl()
		{

			//ボタンを無条件で非活性(Phase2で使用するボタンの為)
			this.btnCustomerSituationListInquiry.Enabled = false;

		}

		#endregion

	}

}
