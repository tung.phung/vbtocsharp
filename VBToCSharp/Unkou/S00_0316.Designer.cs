﻿
using Common.UserControl;

namespace Unkou
{
    partial class S00_0316
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S00_0316));
			this.btnMiCheckinYoyakuListInquiry = new ButtonEx();
			this.btnCheckinSituationListInquiry = new ButtonEx();
			this.pnlBaseFill.SuspendLayout();
			this.SuspendLayout();
			//
			//pnlBaseFill
			//
			this.pnlBaseFill.Controls.Add(this.btnCheckinSituationListInquiry);
			this.pnlBaseFill.Controls.Add(this.btnMiCheckinYoyakuListInquiry);
			//
			//btnMiCheckinYoyakuListInquiry
			//
			this.btnMiCheckinYoyakuListInquiry.AutoSize = true;
			this.btnMiCheckinYoyakuListInquiry.BackColor = System.Drawing.Color.Transparent;
			this.btnMiCheckinYoyakuListInquiry.ExistError = false;
			this.btnMiCheckinYoyakuListInquiry.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnMiCheckinYoyakuListInquiry.ForeColor = System.Drawing.Color.Black;
			this.btnMiCheckinYoyakuListInquiry.GuideMessage = "";
			this.btnMiCheckinYoyakuListInquiry.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnMiCheckinYoyakuListInquiry.GuideMessageDetail"));
			this.btnMiCheckinYoyakuListInquiry.Location = new System.Drawing.Point(12, 8);
			this.btnMiCheckinYoyakuListInquiry.Name = "btnMiCheckinYoyakuListInquiry";
			this.btnMiCheckinYoyakuListInquiry.NotNull = false;
			this.btnMiCheckinYoyakuListInquiry.Size = new System.Drawing.Size(394, 27);
			this.btnMiCheckinYoyakuListInquiry.TabIndex = 1;
			this.btnMiCheckinYoyakuListInquiry.Text = "未チェックイン予約一覧照会";
			this.btnMiCheckinYoyakuListInquiry.UseVisualStyleBackColor = true;
			//
			//btnCheckinSituationListInquiry
			//
			this.btnCheckinSituationListInquiry.AutoSize = true;
			this.btnCheckinSituationListInquiry.BackColor = System.Drawing.Color.Transparent;
			this.btnCheckinSituationListInquiry.ExistError = false;
			this.btnCheckinSituationListInquiry.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F);
			this.btnCheckinSituationListInquiry.ForeColor = System.Drawing.Color.Black;
			this.btnCheckinSituationListInquiry.GuideMessage = "";
			this.btnCheckinSituationListInquiry.GuideMessageDetail = (System.Collections.Generic.List<string>)(resources.GetObject("btnCheckinSituationListInquiry.GuideMessageDetail"));
			this.btnCheckinSituationListInquiry.Location = new System.Drawing.Point(12, 41);
			this.btnCheckinSituationListInquiry.Name = "btnCheckinSituationListInquiry";
			this.btnCheckinSituationListInquiry.NotNull = false;
			this.btnCheckinSituationListInquiry.Size = new System.Drawing.Size(394, 27);
			this.btnCheckinSituationListInquiry.TabIndex = 2;
			this.btnCheckinSituationListInquiry.Text = "チェックイン状況一覧照会";
			this.btnCheckinSituationListInquiry.UseVisualStyleBackColor = true;
			//
			//S00_0316
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(8.0F, 15.0F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 862);
			this.F10Key_Visible = true;
			this.F11Key_Visible = true;
			this.F12Key_Visible = true;
			this.F1Key_Visible = true;
			this.F2Key_Visible = true;
			this.F3Key_Visible = true;
			this.F4Key_Visible = true;
			this.F5Key_Visible = true;
			this.F6Key_Visible = true;
			this.F7Key_Visible = true;
			this.F8Key_Visible = true;
			this.F9Key_Visible = true;
			this.Name = "S00_0316";
			this.setTitleVisible = true;
			this.setWindowTitle = "S00_0316";
			this.Text = "S00_0316";
			this.pnlBaseFill.ResumeLayout(false);
			this.pnlBaseFill.PerformLayout();
			this.ResumeLayout(false);

		}

		internal ButtonEx btnMiCheckinYoyakuListInquiry;
		internal ButtonEx btnCheckinSituationListInquiry;

		#endregion
	}
}