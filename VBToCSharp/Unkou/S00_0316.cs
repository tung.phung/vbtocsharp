﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unkou
{
	/// <summary>
	/// S00_0316 運行サブメニュー（チェックイン）
	/// </summary>
	public partial class S00_0316 : FormBase
	{

		#region イベント

		/// <summary>
		/// フォーム起動時の独自処理
		/// </summary>
		protected override void StartupOrgProc()
		{

			//画面上ボタン制御
			this.setBtnControl();

			//画面初期化
			this.setControlInitiarize();

			//フォーカス設定
			this.ActiveControl = this.btnMiCheckinYoyakuListInquiry;

		}

		#region フッタ

		/// <summary>
		/// F2：戻るボタン押下イベント
		/// </summary>
		protected override void btnF2_ClickOrgProc()
		{

			this.Close();
		}

		#endregion

		#region ボタン

		/// <summary>
		/// 未チェックイン予約一覧照会ボタン実行時のイベント
		/// </summary>
		private void btnMiCheckinYoyakuListInquiry_Click(object sender, EventArgs e)
		{
			//未チェックイン予約一覧照会へ遷移
			//Using form As New S04_0201
			//    form.ShowDialog()
			//End Using
		}

		/// <summary>
		/// チェックイン状況一覧照会ボタン実行時のイベント
		/// </summary>
		private void btnCheckinSituationListInquiry_Click(object sender, EventArgs e)
		{
			//チェックイン状況一覧照会へ遷移
			//Using form As New S04_0203
			//    form.ShowDialog()
			//End Using
		}

		#endregion

		#endregion

		#region メソッド

		/// <summary>
		/// 画面初期化
		/// </summary>
		private void setControlInitiarize()
		{

			//ベースフォームの設定
			this.setFormId = "S00_0316";
			this.setTitle = "運行サブメニュー（チェックイン）";

			//Visible
			this.F1Key_Visible = false;
			this.F2Key_Visible = true;
			this.F3Key_Visible = false;
			this.F4Key_Visible = false;
			this.F5Key_Visible = false;
			this.F6Key_Visible = false;
			this.F7Key_Visible = false;
			this.F8Key_Visible = false;
			this.F9Key_Visible = false;
			this.F10Key_Visible = false;
			this.F11Key_Visible = false;
			this.F12Key_Visible = false;

			//Text
			this.F2Key_Text = "F2:戻る";

		}

		/// <summary>
		/// 画面上ボタン制御
		/// </summary>
		private void setBtnControl()
		{

			//ボタンを無条件で非活性(Phase2で使用するボタンの為)
			this.btnCheckinSituationListInquiry.Enabled = false;
			this.btnMiCheckinYoyakuListInquiry.Enabled = false;

		}

		#endregion

	}
}
