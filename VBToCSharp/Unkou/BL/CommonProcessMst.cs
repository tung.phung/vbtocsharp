﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Unkou.Common;

/// <summary>

/// ''' 共通処理

/// ''' </summary>

/// ''' <remarks></remarks>
public static class CommonProcessMst
{

    // #Region "変数"
    // Private clsMessageOut As New MessageOut()
    // Public XmlFilePath As String
    // Public XmlFilePath_Guide As String
    // Public DataStudioId As String = Nothing
    // Public DataStudioPassword As String = Nothing
    // Public ExclusionMojiCd As String = String.Empty      'AS用
    // #End Region

    // #Region "メッセージ関連"
    // Public Function createFactoryMsg() As MessageOut
    // If XmlFilePath = Nothing Then
    // XmlFilePath = System.IO.Directory.GetCurrentDirectory
    // End If
    // If XmlFilePath_Guide = Nothing Then
    // XmlFilePath_Guide = System.IO.Directory.GetCurrentDirectory
    // End If
    // clsMessageOut.XmlFilePath = XmlFilePath
    // clsMessageOut.XmlFilePath_Guide = XmlFilePath_Guide
    // Return clsMessageOut
    // End Function
    // #End Region

    // #Region " テキストボックス関連 "

    // #Region " AS400_OpenFieldのための文字列チェック処理 "
    // ''' <summary>
    // ''' 'AS/400用の文字列のバイト数チェック
    // ''' </summary>
    // ''' <param name="pChkString"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function chkByteForAS400(ByVal pChkString As String) As Integer

    // Dim totalByte As Integer = 0     '引数である文字列のバイト数
    // Dim beforeCharByte As Integer = 1       '1:半角/2:全角（バイト数）
    // Dim strChar As String
    // Dim charByte As Integer

    // For i As Integer = 1 To pChkString.Length
    // strChar = Mid(pChkString, i, 1)
    // charByte = LenB(strChar)
    // If charByte <> beforeCharByte Then
    // totalByte = totalByte + 1
    // End If

    // totalByte = totalByte + charByte
    // beforeCharByte = charByte
    // Next

    // If beforeCharByte = 2 Then
    // totalByte = totalByte + 1
    // End If

    // Return totalByte

    // End Function

    // ''' <summary>バイト数を取得</summary>
    // Public Function LenB(ByVal stTarget As String) As Integer
    // Return System.Text.Encoding.GetEncoding("Shift_JIS").GetByteCount(stTarget)
    // End Function

    // ''' <summary>Mid関数のバイト版。文字数と位置をバイト数で指定して文字列を切り抜く。</summary>
    // ''' <param name="_str">対象の文字列</param>
    // ''' <param name="_Start">切り抜き開始位置。全角文字を分割するよう位置が指定された場合、戻り値の文字列の先頭は意味不明の半角文字となる。</param>
    // ''' <param name="_Length">切り抜く文字列のバイト数</param>
    // ''' <returns>切り抜かれた文字列</returns>
    // ''' <remarks>最後の１バイトが全角文字の半分になる場合、その１バイトは無視される。</remarks>
    // Public Function midForAS400(ByVal _str As String, ByVal _Start As Integer, Optional ByVal _Length As Integer = 0) As String
    // Dim _totalByte As Integer = 0               '引数である文字列のバイト数
    // Dim _beforeCharByte As Integer = 1          '1:半角/2:全角（バイト数）
    // Dim _strChar As String = String.Empty       '引数である文字列の1文字を格納
    // Dim _charByte As Integer = 0                '_strCharのバイト数を格納
    // Dim _ShiftCode As Integer = 1               'シフトコードバイト数を保持（固定）
    // Dim _ShiftOutCode As Integer = 0            'シフトアウトコードを格納
    // Dim _returnValue As String = String.Empty   '戻り値を格納

    // If _str = "" Then
    // Return ""
    // End If

    // For _charCnt As Integer = 1 To _str.Length
    // _strChar = Mid(_str, _charCnt, 1)
    // _charByte = LenB(_strChar)
    // If _charByte <> _beforeCharByte Then
    // '全角⇔半角切替時↓
    // _totalByte = _totalByte + _ShiftCode + _charByte
    // Else
    // '全角または半角連続↓
    // _totalByte = _totalByte + _charByte
    // End If

    // '前回バイト数保持↓
    // _beforeCharByte = _charByte

    // If _charByte = 2 Then
    // '全角文字＝ShiftOutを加味↓
    // _ShiftOutCode = 1
    // Else
    // _ShiftOutCode = 0
    // End If

    // If (_totalByte + _ShiftOutCode) <= _Length Then
    // '指定バイト数以内↓
    // _returnValue &= _strChar
    // Else
    // '指定バイト数以上↓
    // Exit For
    // End If
    // Next

    // Return _returnValue

    // End Function
    // #End Region

    // #Region " JIS変換チェック処理"
    // ''' <summary>JISへ変換可能かチェックする</summary>
    // Public Function sjisCheck(ByVal checkString As String) As String
    // Dim errString As String = String.Empty
    // Dim chgbyte() As Byte
    // Dim chgStatus As Boolean
    // Dim enc As Encoding

    // 'SHIFT-JISへの変換
    // '変換不可文字はExceptionを発生させる
    // enc = Encoding.GetEncoding("shift_jis", New EncoderExceptionFallback, New DecoderExceptionFallback)

    // For Each c As Char In checkString
    // chgStatus = False
    // Try
    // chgbyte = enc.GetBytes(c)
    // 'VB.NETでのshift(-jisはMS932のため純粋なSHIFT-JISの対応外をチェックする)
    // If chgbyte.Length = 1 Then
    // Select Case chgbyte(0)
    // Case Is <= &HDF
    // chgStatus = True
    // End Select
    // Else
    // Dim num As Integer = chgbyte(0)
    // num = num << 8
    // num = num Or chgbyte(1)
    // Select Case num
    // Case &H8740 To &H879E   '13区
    // '一部IBM拡張文字は許可する(ASはSHIFT-JIS+IBM拡張
    // If (&H8754 <= num And num <= &H875D) _
    // OrElse num = &H8782 _
    // OrElse num = &H8784 _
    // OrElse num = &H878A Then
    // chgStatus = True
    // End If
    // Case &HED40 To &HED9E   '89区
    // Case &HED9F To &HEDFC   '90区
    // Case &HEE40 To &HEE9E   '91区
    // Case &HEE9F To &HEEFC   '92区
    // 'Case &HFA40 To &HFA9E   '115区
    // 'Case &HFA9F To &HFAFC   '116区
    // 'Case &HFB40 To &HFB9E   '117区
    // 'Case &HFB9F To &HFBFC   '118区
    // 'Case &HFC40 To &HFC4B   '119区
    // Case &HFA40 To &HFC4B   '115区～119区
    // If (&HFA55 <> num) Then
    // chgStatus = True
    // End If
    // Case Else
    // chgStatus = True
    // End Select
    // End If
    // If chgStatus = False Then
    // errString += c
    // Else
    // If ExclusionMojiCd.IndexOf(c) <> -1 Then
    // errString += c
    // End If
    // End If
    // Catch ex As Exception
    // errString += c
    // End Try
    // Next

    // Return errString

    // End Function

    // ''' <summary>unicode⇒shift-jisへ変換可能かチェックする</summary>
    // Public Function unicodeCheck(ByVal checkString As String) As String
    // Dim errString As String = String.Empty
    // Dim chgbyte() As Byte
    // Dim chgStatus As Boolean
    // Dim enc As Encoding

    // 'SHIFT-JISへの変換
    // '変換不可文字はExceptionを発生させる
    // enc = Encoding.GetEncoding("shift_jis", New EncoderExceptionFallback, New DecoderExceptionFallback)

    // For Each c As Char In checkString
    // chgStatus = False
    // Try
    // chgbyte = enc.GetBytes(c)
    // Catch ex As Exception
    // errString += c
    // End Try
    // Next

    // Return errString
    // End Function

    // #End Region

    // #Region "NULL変換"
    // ''' <summary>
    // ''' DBNullかチェックし、DBNullだったらdefaultValueの値を返す
    // ''' </summary>
    // ''' <param name="pObj"></param>
    // ''' <param name="defaultValue">pObjがDBNullだった場合に返す値</param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function Nvl(ByVal pObj As Object, Optional ByVal defaultValue As Object = Nothing) As Object

    // Dim returnValue As Object = defaultValue

    // If Not IsDBNull(pObj) Then
    // returnValue = pObj
    // End If

    // Return returnValue

    // End Function

    // #End Region

    // #Region "Nothing変換"
    // ''' <summary>
    // ''' 値がNothingまたはString.Emptyの時、defaultValueの値を返す
    // ''' </summary>
    // ''' <param name="pString"></param>
    // ''' <param name="defaultValue">値がNothingまたはString.Emptyの時に返す値</param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getEmptyValue(ByVal pString As String, Optional ByVal defaultValue As String = "") As String

    // Dim returnValue As String = defaultValue

    // If pString IsNot Nothing AndAlso
    // pString <> String.Empty Then
    // returnValue = pString
    // End If

    // Return returnValue

    // End Function
    // #End Region

    // #End Region

    // #Region "数値ボックス関連"

    // #Region "Numbwe.Value変換（Nullable Decimal）"
    // Public Function getValue(ByVal val As Decimal?) As Decimal
    // If val.HasValue = False Then
    // Return 0D
    // Else
    // Return CDec(val)
    // End If
    // End Function
    // #End Region

    // #End Region

    // #Region " チェックボックス関連 "

    // ''' <summary>
    // ''' Trueを1、Falseを0で返す
    // ''' </summary>
    // ''' <param name="pValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getCheckedValue(ByVal pValue As Boolean) As Integer

    // Dim returnValue As Integer = 0

    // If pValue Then
    // returnValue = 1
    // End If

    // Return returnValue

    // End Function

    // ''' <summary>
    // ''' 1をTrue、0をFalseで返す
    // ''' </summary>
    // ''' <param name="pValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getCheckBoxValue(ByVal pValue As Integer) As Boolean

    // Dim returnValue As Boolean = True

    // If pValue = 0 Then
    // returnValue = False
    // End If

    // Return returnValue

    // End Function

    // ''' <summary>
    // ''' 削除チェックボックス専用：String.EmptyならFalseを返す
    // ''' </summary>
    // ''' <param name="pValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getCheckBoxValueForDeleteDate(ByVal pValue As String) As Boolean

    // Dim returnValue As Boolean = True

    // If pValue = String.Empty Then
    // returnValue = False
    // End If

    // Return returnValue

    // End Function

    // ''' <summary>
    // ''' 削除フラグのセット値を取得する
    // ''' </summary>
    // ''' <param name="checkedValue"></param>
    // ''' <param name="oldValue">前回値の値</param>
    // ''' <returns>チェックされていたら前回値、前回値がなければ本日日付を返す</returns>
    // ''' <remarks></remarks>
    // Public Function getDeleteValue(ByVal checkedValue As Boolean, ByVal oldValue As String) As String

    // Dim returnValue As String = String.Empty

    // If checkedValue Then
    // If oldValue <> String.Empty Then
    // returnValue = oldValue
    // Else
    // returnValue = Today.Date.ToString("yyyyMMdd")
    // End If
    // End If

    // Return returnValue

    // End Function

    // #End Region

    // #Region " コンボボックス関連 "
    // ''' <summary>
    // ''' グリッドに設置したコンボボックスへデータを格納するSortedListを作成する
    // ''' </summary>
    // ''' <param name="dataTableValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getComboboxData(ByVal dataTableValue As DataTable, Optional ByVal nullRecord As Boolean = False) As SortedList(Of String, String)
    // Dim returnValue As New SortedList(Of String, String)

    // If nullRecord = True Then
    // returnValue.Add("", "")
    // End If

    // For idxCountRow As Integer = 0 To dataTableValue.Rows.Count - 1

    // returnValue.Add(dataTableValue.Rows(idxCountRow).Item(ComboBoxCdType.CODE_VALUE.ToString).ToString,
    // dataTableValue.Rows(idxCountRow).Item(ComboBoxCdType.CODE_NAME.ToString).ToString)

    // Next

    // Return returnValue

    // End Function
    // ''' <summary>
    // ''' Enumからグリッドに設置したコンボボックスへデータを格納するSortedListを作成
    // ''' </summary>
    // ''' <param name="systemTypeValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getComboboxData(ByVal systemTypeValue As System.Type, Optional ByVal nullRecord As Boolean = False) As SortedList(Of String, String)
    // Dim returnValue As New SortedList(Of String, String)

    // If nullRecord = True Then
    // returnValue.Add("", "")
    // End If

    // 'For Each objectCount As Integer In System.Enum.GetValues(systemTypeValue)
    // For Each objectCount As Object In System.Enum.GetValues(systemTypeValue)

    // 'returnValue.Add(objectCount.ToString, deleteUnderScore(System.Enum.GetName(systemTypeValue, objectCount)))
    // returnValue.Add(CInt(objectCount), getEnumAttrValue(objectCount))

    // Next

    // Return returnValue

    // End Function

    // ''' <summary>
    // ''' Enumからグリッドに設置したコンボボックスへデータを格納するSortedListを作成
    // ''' </summary>
    // ''' <param name="systemTypeValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getComboboxData_MojiColor_Kikaku(ByVal systemTypeValue As System.Type, Optional ByVal nullRecord As Boolean = False) As SortedList(Of String, String)
    // Dim returnValue As New SortedList(Of String, String)

    // If nullRecord = True Then
    // returnValue.Add("", "")
    // End If

    // For Each objectCount As Integer In System.Enum.GetValues(systemTypeValue)

    // returnValue.Add(objectCount.ToString, CommonType_MojiColValue.MojiColorTypeKikaku_Value(CType(objectCount, MojiColorType_Kikaku)))

    // Next

    // Return returnValue

    // End Function

    // ''' <summary>
    // ''' Enumからグリッドに設置したコンボボックスへデータを格納するSortedListを作成
    // ''' </summary>
    // ''' <param name="systemTypeValue"></param>
    // ''' <returns></returns>
    // ''' <remarks></remarks>
    // Public Function getComboboxData_MojiColor_Teiki(ByVal systemTypeValue As System.Type, Optional ByVal nullRecord As Boolean = False) As SortedList(Of String, String)
    // Dim returnValue As New SortedList(Of String, String)

    // If nullRecord = True Then
    // returnValue.Add("", "")
    // End If

    // For Each objectCount As Integer In System.Enum.GetValues(systemTypeValue)

    // returnValue.Add(objectCount.ToString, CommonType_MojiColValue.MojiColorType_Teiki_Value(CType(objectCount, MojiColorType_Teiki)))

    // Next

    // Return returnValue

    // End Function

    /// <summary>
    ///     ''' Enumから、画面に設置したコンボボックスへデータを格納するDataTableを作成
    ///     ''' </summary>
    ///     ''' <param name="typ"></param>
    ///     ''' <param name="nullRecord"></param>
    ///     ''' <returns></returns>
    ///     ''' <remarks></remarks>
    public static DataTable getComboboxDataOfDatatable(System.Type typ, bool nullRecord = false)
    {
        DataTable returnValue = new DataTable();

        returnValue.Columns.Add("CODE_VALUE");
        returnValue.Columns.Add("CODE_NAME");

        if (nullRecord == true)
            returnValue.Rows.Add(new object[] { "", "" });

        foreach (object obj in System.Enum.GetValues(typ))
            returnValue.Rows.Add(System.Convert.ToInt32(obj), getEnumAttrValue(obj));

        return returnValue;
    }

    public static string getEnumAttrValue(object obj)
    {
        Type typ = obj.GetType();
        string name = System.Enum.GetName(typ, obj);
        System.Reflection.FieldInfo f = typ.GetField(obj.ToString());
        object[] attributes = f.GetCustomAttributes(typeof(ValueAttribute), false);
        if (attributes.Count() == 0)
            return deleteUnderScore(name);
        else
            return deleteUnderScore(attributes[0].ToString());
    }

    /// <summary>
    ///     ''' Enumから、画面に設置したコンボボックスへデータを格納するDataTableを作成
    ///     ''' </summary>
    ///     ''' <param name="systemTypeValue"></param>
    ///     ''' <param name="nullRecord"></param>
    ///     ''' <returns></returns>
    ///     ''' <remarks></remarks>
    public static DataTable getComboboxDataOfDatatable_old(System.Type systemTypeValue, bool nullRecord = false)
    {
        DataTable returnValue = new DataTable();

        returnValue.Columns.Add("CODE_VALUE");
        returnValue.Columns.Add("CODE_NAME");

        if (nullRecord == true)
            returnValue.Rows.Add(new object[] { "", "" });

        foreach (int objectCount in System.Enum.GetValues(systemTypeValue))

            returnValue.Rows.Add(objectCount.ToString(), deleteUnderScore(System.Enum.GetName(systemTypeValue, objectCount)));

        return returnValue;
    }

    // #End Region

    /// <summary>
    ///     ''' 先頭の_(アンダースコア)を削除
    ///     ''' ※列挙型で使用を想定
    ///     ''' </summary>
    ///     ''' <param name="value"></param>
    ///     ''' <returns></returns>
    ///     ''' <remarks></remarks>
    public static string deleteUnderScore(string value)
    {
        if (value.Substring(0, 1) == "_")
        {
            return value.Substring(1, value.Count());
        }
        else
        {
            return value;
        }
    }
}
