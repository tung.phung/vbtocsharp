﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unkou.BL
{
    public class S04_0105ParamData
    {
        /// <summary>
        ///     ''' 予約区分
        ///     ''' </summary>
        ///     ''' <returns></returns>
        public string YoyakuKbn { get; set; }

        /// <summary>
        ///     ''' 予約番号
        ///     ''' </summary>
        ///     ''' <returns></returns>
        public int YoyakuNo { get; set; }
    }

}
