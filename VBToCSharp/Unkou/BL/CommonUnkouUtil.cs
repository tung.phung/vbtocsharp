﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Unkou.UserControl;
using static Unkou.Common.FixedCd;

/// <summary>

/// ''' マスタメンテナンス共通処理クラス

/// ''' </summary>
public class CommonUnkouUtil
{

    /// <summary>
    ///     ''' WHERE文字列作成
    ///     ''' </summary>
    ///     ''' <param name="grd">対象Grid</param>
    ///     ''' <param name="keys">Key配列</param>
    public static string MakeWhere(FlexGridEx grd, string[] keys)
    {
        //string whereString = "";
        //{
        //    var withBlock = grd;
        //    if (withBlock.Row >= 0 & withBlock.Rows.Count > 1)
        //    {
        //        foreach (string s in keys)
        //        {
        //            if (withBlock.Item(withBlock.Row, s).ToString != string.Empty)
        //            {
        //                if (whereString != string.Empty)
        //                    whereString += " AND ";
        //                whereString += s + " = '" + withBlock.Item(withBlock.Row, s).ToString + "'";
        //            }
        //        }
        //    }
        //}

        return string.Empty;
    }

    /// <summary>
    ///     ''' コンボの共通的なプロパティ設定
    ///     ''' </summary>
    ///     ''' <param name="cmbParam"></param>
    ///     ''' <param name="dataSourceParram"></param>
    public static void setComboCommonProperty(ComboBoxEx cmbParam, DataTable dataSourceParram)
    {
        {
            var withBlock = cmbParam;
            withBlock.DataSource = dataSourceParram;
            withBlock.DropDown.AllowResize = false;
            withBlock.ListHeaderPane.Visible = false;
            //withBlock.ListColumns(ComboBoxCdType.CODE_VALUE).Visible = false;
            //withBlock.ListColumns(ComboBoxCdType.CODE_NAME).Width = withBlock.Width;
            //withBlock.TextSubItemIndex = ComboBoxCdType.CODE_NAME;
        }
    }


    /// <summary>
    ///     ''' コンボボックスのデータ設定
    ///     ''' </summary>
    ///     ''' <param name="CdBunrui"></param>
    ///     ''' <param name="ComboName"></param>
    ///     ''' <param name="NullRecord"></param>
    ///     ''' <param name="arraySiireSakiShubetsu"></param>
    public static void setComboBox(CdBunruiType CdBunrui, ref ComboBoxEx ComboName, bool NullRecord, string[] arraySiireSakiShubetsu = null)
    {

        // パラメータ
        //Hashtable paramInfoList = new Hashtable();

        //// DataAccessクラス生成
        //Code_DA dataAccess = new Code_DA();

        //// 戻り値
        //DataTable returnValue = null/* TODO Change to default(_) if this is not a reference type */;

        //returnValue = dataAccess.GetCodeMasterData(System.Convert.ToString(CdBunrui), arraySiireSakiShubetsu, NullRecord);
        //ComboName.DataSource = returnValue;
        //{
        //    var withBlock = ComboName;
        //    withBlock.ListColumns(ComboBoxCdType.CODE_NAME).Visible = true;
        //    withBlock.ListColumns(ComboBoxCdType.CODE_NAME).Width = withBlock.Width;
        //    withBlock.ListColumns(ComboBoxCdType.CODE_VALUE).Visible = false;
        //    withBlock.TextSubItemIndex = ComboBoxCdType.CODE_NAME;
        //    withBlock.ValueSubItemIndex = ComboBoxCdType.CODE_VALUE;
        //}
    }
}
